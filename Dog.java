public class Dog {
    private String name;
    private int energy;
    private int hunger;
    private int boredum;

    public Dog(String name, int energy, int hunger, int boredum) {
        this.energy = 0;
        this.hunger = 0;
        this.boredum = 0;
    }

    public boolean takeNap() {
        if (this.energy < 50 || this.boredum < 50) {
            System.out.println("daggy tired");
            return false;
        } else if (this.energy > 50 || this.boredum > 50) {
            this.energy = this.energy + 20;
            this.hunger = this.hunger + 5;
            this.boredum = this.boredum + 10;
            System.out.println("daggy napped");
            return true;
        } else {
            System.out.println("daggy bored");
            return false;
        }
    }

    public void play() {
        if (this.boredum < 30) {
            throw new ArithmeticException("daggy boredom cannot be less than 0");
        } else {
            this.boredum = this.boredum - 30;
            this.hunger = this.hunger + 5;
            System.out.println("daggy play hooman");
        }
    }

    public boolean playWith(Human hooman) {
        if (this.energy > 50) {
            this.energy = this.energy - 50;
            hooman.play();
            return true;
        } else {
            System.out.println("doggo cannot play (no energi)");
            return false;
        }

    }

    public void eat(){
        this.hunger = this.hunger - 30;
        if(this.hunger < 0){
            this.hunger = 0;
        }
        System.out.println("daggy: " + this.name + " nommed");
    }
}