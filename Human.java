public class Human{
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String name){
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
        this.name = name;
    }

    public boolean doWork(){
        if(this.hunger > 50 || this.boredom > 50){
            if(this.hunger > 50){
                System.out.println(this.name + " is too hungry to work");
            }
            if(this.boredom > 50){
                System.out.println(this.name + " is too bored to work");
            }
            return false;
        }
        this.money = this.money + 20;
        this.hunger = this.hunger + 5;
        this.boredom = this.boredom + 10;
        System.out.println(this.name + " has completed a long days work");
        return true;
    }

    public void play() {
        if (this.boredom < 30) {
            throw new ArithmeticException("Hooman's boredum cannot be less than 0.");
        } else {
            this.boredom = this.boredom - 30;
            this.hunger = this.hunger + 5;
            System.out.println("hooman play with doggo");
        }
    }

    public void eat(){
        this.hunger = this.hunger - 30;
        if(this.hunger < 0){
            this.hunger = 0;
        }
        System.out.println(this.name + " ate");
    }

    public boolean feed(Dog dog){
        if(this.money > 50){
            this.money = this.money - 50;
            System.out.println(this.name + " bought food");
            eat();
            dog.eat();
            return true;
        }
        return false;
    }
}